<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="single-wrapper">
  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
    <div class="row">
      <div class="col-md content-area" id="primary">
      <main class="site-main" id="main">
        <div class="entry-content">
        <?php if ( have_posts() ) : ?>
          <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; // end of the loop. ?>
        <?php endif; ?>
        </div>
      </div><?php // need this extra closing tag ?>
    </div> 
  </div>

<?php get_footer(); ?>
