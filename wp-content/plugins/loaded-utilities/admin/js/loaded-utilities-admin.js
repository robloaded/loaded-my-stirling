(function( $ ) {
  'use strict';

  $( window ).load(function() {
    console.log('admin js loading...');
    if ( $('#tsf-inpost-box').hasClass('closed') === false ) {
      $('#tsf-inpost-box').addClass('closed');
    }
  });

})( jQuery );
