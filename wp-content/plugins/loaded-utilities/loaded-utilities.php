<?php

/**
 *
 * @link              https://www.loadedcommunications.com.au
 * @since             1.0.4
 * @package           Loaded_Utilities
 *
 * @wordpress-plugin
 * Plugin Name:       Loaded Utilities
 * Plugin URI:        https://www.loadedcommunications.com.au
 * Description:       Collection of utility functions and admin customisation for Loaded websites.
 * Version:           1.0.4
 * Author:            Loaded Communications
 * Author URI:        https://www.loadedcommunications.com.au
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       loaded-utilities
 * Domain Path:       /languages
 */

//  namespace lewiscowles\WordPress\Compat\FileTypes;

// If this file is called directly, abort.
if (!defined('WPINC')) {
  die;
}

/**
 * Currently plugin version.
 */
define('LOADED_UTILITIES_VERSION', '1.0.4');
define('ACFGFS_API_KEY', 'AIzaSyBGloJDZahHpjKXY_UyYMKxrk_bN_H248w');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-loaded-utilities-activator.php
 */

if ( version_compare( PHP_VERSION, '7', '<' ) ) {
  ?>
  <div id="error-page">
      <p>This plugin requires PHP 7 or higher. Please contact your hosting provider about upgrading your
          server software. Your PHP version is <b><?php echo PHP_VERSION; ?></b></p>
  </div>
  <?php
  die();
}

require __DIR__ . '/vendor/autoload.php';

use Leafo\ScssPhp\Compiler;
use SSNepenthe\ColorUtils\Colors\Rgba as R;
use SSNepenthe\ColorUtils\Colors\Color as C;
use SSNepenthe\ColorUtils\Colors\ColorFactory;
use function SSNepenthe\ColorUtils\{
    alpha, blue, brightness, brightness_difference, color, color_difference,
    contrast_ratio, green, hsl, hsla, hue, is_bright, is_light, lightness,
    looks_bright, name, opacity, perceived_brightness, red, relative_luminance, rgb,
    rgba, saturation, lighten
};
use function lewiscowles\Utils\FileSystem\Extension\fixExtensionIfNeeded;

function activate_loaded_utilities() {
  require_once plugin_dir_path(__FILE__) . 'includes/class-loaded-utilities-activator.php';
  Loaded_Utilities_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-loaded-utilities-deactivator.php
 */
function deactivate_loaded_utilities() {
  require_once plugin_dir_path(__FILE__) . 'includes/class-loaded-utilities-deactivator.php';
  Loaded_Utilities_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_loaded_utilities');
register_deactivation_hook(__FILE__, 'deactivate_loaded_utilities');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-loaded-utilities.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.4
 */

function deregister_jquery_migrate( $scripts ) {
	if ( ! is_admin() && ! empty( $scripts->registered['jquery'] ) ) {
		$jquery_dependencies = $scripts->registered['jquery']->deps;
		$scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
	}
}
add_action( 'wp_default_scripts', 'deregister_jquery_migrate' );

//  class SVGSupport {

//   function init() {
//     add_action( 'admin_init', [ $this, 'add_svg_upload' ], 75 );
//     add_action( 'admin_head', [ $this, 'custom_admin_css' ], 75 );
//     add_action( 'load-post.php', [ $this, 'add_editor_styles' ], 75 );
//     add_action( 'load-post-new.php', [ $this, 'add_editor_styles' ], 75 );
//     add_action( 'after_setup_theme', [ $this, 'theme_prefix_setup' ], 75 );
//     add_filter( 'wp_check_filetype_and_ext', [ $this, 'fix_mime_type_svg' ], 75, 4 );
//     add_filter( 'wp_update_attachment_metadata', [ $this, 'ensure_svg_metadata' ], 10, 2 );
//   }

//   public function add_svg_upload() {
//     add_action( 'wp_ajax_adminlc_mce_svg.css', [ $this, 'tinyMCE_svg_css' ], 10 );
//     add_filter( 'image_send_to_editor', [ $this, 'remove_dimensions_svg' ], 10, 1 );
//     add_filter( 'upload_mimes', [ $this, 'filter_mimes' ], 10, 1 );
//   }

//   public function custom_admin_css() {
//     echo '<style>';
//     $this->custom_css();
//     echo '</style>';
//   }

//   public function add_editor_styles() {
//     add_filter( 'mce_css', [ $this, 'filter_mce_css' ] );
//   }

//   public function theme_prefix_setup() {
//     $existing = get_theme_support( 'custom-logo' );
//     if ( $existing ) {
//       $existing = current( $existing );
//       $existing['flex-width'] = true;
//       $existing['flex-height'] = true;
//       add_theme_support( 'custom-logo', $existing );
//     }
//   }

//   public function fix_mime_type_svg( $data=null, $file=null, $filename=null, $mimes=null ) {
//     $OriginalExtension = (isset( $data['ext'] ) ? $data['ext'] : '');
//                 $ext = fixExtensionIfNeeded($OriginalExtension, $filename);
//     if( $ext === 'svg' ) {
//       $data['type'] = 'image/svg+xml';
//       $data['ext'] = 'svg';
//     }
//     return $data;
//   }

//   public function ensure_svg_metadata( $data, $id ) {
//     $attachment = get_post( $id );
//     $mime_type = $attachment->post_mime_type;

//     if ( $mime_type == 'image/svg+xml' ) {
//       if( $this->missingOrInvalidSVGDimensions( $data ) ) {
//         $xml = simplexml_load_file( get_attached_file( $id ) );
//         $attr = $xml->attributes();
//         $viewbox = explode( ' ', $attr->viewBox );

//         $this->fillSVGDimensions( $viewbox, $attr, $data, 'width', 2 );
//         $this->fillSVGDimensions( $viewbox, $attr, $data, 'height', 3 );
//       }
//     }
//     return $data;
//   }

//   public function tinyMCE_svg_css() {
//     header( 'Content-type: text/css' );
//     $this->custom_css();
//     exit();
//   }

//   public function remove_dimensions_svg( $html = '' ) {
//     return str_ireplace( [ " width=\"1\"", " height=\"1\"" ], "", $html );
//   }

//   public function filter_mimes( $mimes = [] ){
//     $mimes[ 'svg' ] = 'image/svg+xml';
//     return $mimes;
//   }


//   public function filter_mce_css( $mce_css ) {
//     global $current_screen;
//     $mce_css .= ', ' . get_admin_url( 'admin-ajax.php?action=adminlc_mce_svg.css' );
//     return $mce_css;
//   }

//   protected function custom_css() {
//     echo 'img[src$=".svg"]:not(.emoji) { width: 100% !important; height: auto !important; }';
//   }

//   protected function missingOrInvalidSVGDimensions( $data ) {
//     if(!is_array($data)) return true;
//     if(!isset($data['width']) || !isset($data['height']) ) return true;
//     if(is_nan($data['width']) || is_nan($data['height']) ) return true;
//     return (
//       empty( $data ) || empty( $data['width'] ) || empty( $data['height'] )
//       ||
//       intval($data['width'] < 1) || intval($data['height'] < 1)
//     );
//   }

//   protected function fillSVGDimensions( $viewbox, $attr, &$data, $dimension, $viewboxoffset ) {
//     if ( isset( $attr->{ $dimension } ) ) {
//       $data[ $dimension ] = intval( $attr->{ $dimension } );
//     }
//     if( !isset( $data[ $dimension ] ) ) {
//       $data[ $dimension ] = 0;
//     }
//     if ( is_nan( $data[ $dimension ] ) ) {
//       $data[ $dimension ] = 0;
//     }
//     if ( $data[ $dimension ] < 1 ) {
//       $data[ $dimension ] = count($viewbox) == 4 ? intval( $viewbox[$viewboxoffset] ) : null;
//     }
//   }
// }

// if (defined('ABSPATH')) {
//   require_once(__DIR__.'/vendor/autoload.php');
//   $svg_support = new SVGSupport();
//   $svg_support->init();
// }



if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array(
    'page_title' => 'MyStirling Theme Settings',
    'menu_title' => 'Edit Theme',
    'menu_slug'  => 'theme-general-settings',
    'menu_order' => 20,
    'capability' => 'edit_posts',
    'redirect'   => false,
    'icon_url'   => 'https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/img/theme-edit.png',
  ));
  acf_add_options_sub_page(array(
    'page_title'  => 'Theme Header Settings',
    'menu_title'  => 'Header',
    'parent_slug' => 'theme-general-settings',
  ));
  acf_add_options_sub_page(array(
    'page_title'  => 'Theme Footer Settings',
    'menu_title'  => 'Footer',
    'parent_slug' => 'theme-general-settings',
  ));
  acf_add_options_sub_page(array(
    'page_title'  => 'MyStirling Settings',
    'menu_title'  => 'MyStirling',
    'parent_slug' => 'theme-general-settings',
  ));
}

// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
  $post_types = get_post_types();
  foreach ($post_types as $post_type) {
    if (post_type_supports($post_type, 'comments')) {
      remove_post_type_support($post_type, 'comments');
      remove_post_type_support($post_type, 'trackbacks');
    }
  }
}
add_action('admin_init', 'df_disable_comments_post_types_support');
// Close comments on the front-end
function df_disable_comments_status() {
  return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);
// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
  $comments = array();
  return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);
// Remove comments page in menu
function df_disable_comments_admin_menu() {
  remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');
// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
  global $pagenow;
  if ($pagenow === 'edit-comments.php') {
    wp_redirect(admin_url());exit;
  }
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');
// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
  remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');
// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
  if (is_admin_bar_showing()) {
    remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
  }
}
add_action('init', 'df_disable_comments_admin_bar');

class loaded_color_schemes {

  private $__colors = array(
    'primary', 'flat',
  );

  public function __construct() {
    add_action('admin_init', array($this, 'add_colors'));
  }

  /**
   * Register color schemes.
   */
  public function add_colors() {
    $suffix = is_rtl() ? '-rtl' : '';

    wp_admin_css_color(
      'primary', __('Primary', 'admin_schemes'),
      plugins_url("primary/colors$suffix.css", __FILE__),
      array('#282b48', '#35395c', '#f38135', '#e7c03a'),
      array('base' => '#f1f2f3', 'focus' => '#fff', 'current' => '#fff')
    );

    wp_admin_css_color(
      'flat', __('Flat', 'admin_schemes'),
      plugins_url("flat/colors$suffix.css", __FILE__),
      array('#1F2C39', '#2c3e50', '#1abc9c', '#f39c12'),
      array('base' => '#f1f2f3', 'focus' => '#fff', 'current' => '#fff')
    );
  }
}
global $acs_colors;
$acs_colors = new loaded_color_schemes;

function loaded_util_admin_head() {
  $dir        = plugin_dir_path(__FILE__);
  $admin_css  = file_get_contents($dir . 'css/admin-style.css');
  $admin_js   = file_get_contents($dir . 'admin-scripts.js');
  echo '<style type="text/css">' . $admin_css . '</style>';
  echo '<script type="text/javascript">' . $admin_js . '</script>';
}

add_action('admin_head', 'loaded_util_admin_head');

// Add inline JS in the admin head with the <script> tag

/*
function loaded_util_admin_head() {
echo '<script type=""text/javascript">console.log('admin script')</script>';
}
add_action( 'admin_head', 'loaded_util_admin_head' );*/

function set_default_admin_color($user_id) {
  $args = array(
    'ID'          => $user_id,
    'admin_color' => 'flat',
  );
  wp_update_user($args);
}
add_action('user_register', 'set_default_admin_color');

add_filter('medium-editor-theme', 'medium_editor_color_scheme');

function medium_editor_color_scheme($theme) {
  $theme = 'beagle';
  return $theme;
}

function loaded_acf_save_post( $post_id ) {
  $primary_color = get_field('primary_color', 'option');
  $scss = new Compiler();
  chdir( get_stylesheet_directory() . '/sass' );
  $themeSCSS = file_get_contents( './theme.scss' );
  $returnSCSS = $scss->compile($themeSCSS);
  update_field('test_field', $returnSCSS, 'option');
  // file_put_contents( '../css/theme-compiled.css', $returnSCSS );
  $returnColor = '
  
  primary_color red :   ' . red($primary_color) . '
  primary_color green : ' . green($primary_color) . '
  primary_color blue :  ' . blue($primary_color) . '
  
  ';
  // update_field('test_field', $returnColor, 'option');  
}

add_action('acf/save_post', 'loaded_acf_save_post', 20);

function run_loaded_utilities() {
  $plugin = new Loaded_Utilities();
  $plugin->run();
}

run_loaded_utilities();
