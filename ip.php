<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */

/*
 * 302 Redirect all hits except from whitelisted IP
 * (loaded dev) May 2019
 */

function checkRequestIP() {
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $VISITOR_IP = $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $VISITOR_IP = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    $VISITOR_IP = $_SERVER['REMOTE_ADDR'];
  }
  if ( strlen($VISITOR_IP) > 7 ) {
    return $VISITOR_IP;
  } else {
    return false;
  }
}

if ( checkRequestIP() !== "203.59.97.206" ) :
        if ( $_GET["debug"] === true ) {
            echo ' <br> <pre style="display: block;width: 1200px;max-width: 100%;background: #fbfbfb;color: #000;margin: 16px auto 32px !important;border-radius: 5px;border: 2px solid #f7acac;padding: 4px 16px !important;font-size: 1.125rem;white-space: pre-wrap;word-break: break-all;text-overflow: ellipsis;overflow: hidden;box-shadow: 0 3px 5px -1px rgba(0,0,0,.2), 0 5px 8px 0 rgba(0,0,0,.14), 0 1px 14px 0 rgba(0,0,0,.12);">'; highlight_string("<?php data = " . var_export( $_SERVER , true ) . "; ?>"); echo ' </pre> <br> <br> ';
        } else {
  header("HTTP/1.1 302 Moved Permanently");
  header("Location: https://mystirling.com/verdant");
        }
endif;

if ( checkRequestIP() === "203.59.97.206" ) :

    if ( $_GET["debug"] === true ) {
        echo ' <br> <pre style="display: block;width: 1200px;max-width: 100%;background: #fbfbfb;color: #000;margin: 16px auto 32px !important;border-radius: 5px;border: 2px solid #f7acac;padding: 4px 16px !important;font-size: 1.125rem;white-space: pre-wrap;word-break: break-all;text-overflow: ellipsis;overflow: hidden;box-shadow: 0 3px 5px -1px rgba(0,0,0,.2), 0 5px 8px 0 rgba(0,0,0,.14), 0 1px 14px 0 rgba(0,0,0,.12);">'; highlight_string("<?php data = " . var_export( $_SERVER , true ) . "; ?>"); echo ' </pre> <br> <br> ';
    }
        
/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */

define('WP_USE_THEMES', true);

/** Loads the WordPress Environment and Template */
require( dirname( __FILE__ ) . '/wp-blog-header.php' );

endif;
