<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'admin_mystirling' );

/** MySQL database username */
define( 'DB_USER', 'admin_mystirling' );

/** MySQL database password */
define( 'DB_PASSWORD', 'e3R2l53fT1' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I!&A8&:Tr+KD7LpduM@nG>(#+f1pa+c]B-l. Z[Jw-Yi!y%-gg:alRl2eR6~@Rj*');
define('SECURE_AUTH_KEY',  'q)BT+9Qf^l%eXBi,x-(g#Uc =GxXwo:B!P%L%,h=f@Fnot-J>rW/+Br+TLoI3?<m');
define('LOGGED_IN_KEY',    '*b)>I@nAU{+}Q|9bgLj.vAE/6^/:U3r5oUcCnW8<{5trf|3<p2X:5^,yco!Q|9zQ');
define('NONCE_KEY',        '}obAyX<[+lX^avvsa7dJwZ.]@w9-egMAYDfe~|5?uXgdJ&ryHp!0$xNR^1x:.UM&');
define('AUTH_SALT',        '$;}5K??j9BO,Sp>P}s;>_P_INJxq&Ou{122IZXs>@MQW[XsX@wrW0~o1RCudI#YG');
define('SECURE_AUTH_SALT', 'j|R8Cu{s`NDnU|z;Ng9y;2-i-*QL*lLy||^}Xf~VxsI7z/0_-ZS-Vmp7;:%ZM@ox');
define('LOGGED_IN_SALT',   'Y[j@dmv^vxiE|}Y6? +E;uTI9u`J+rNC5<`pfx!5^~4>hBISf({Ufdg7LK)BgO|-');
define('NONCE_SALT',       'W|COke%n@C1Y GM]k;k`Y#gh|L,e7<wtIM[0m|b-)*bSv|*^a8(Id3uF. >DZ6<Q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

if (strpos($_SERVER['REQUEST_URI'], 'wp-admin') !== false){
  define('CONCATENATE_SCRIPTS', false );
}

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

